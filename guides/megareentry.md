# Mega Rentry

[TOC]

## Info about Rentry
This aims (used very loosely) to list many Rentry Links in one place and mirror some popular guides. 
This will develop slowly and overtime so please bear with me. (I am a single user working on this)
Most the of Linked data here, has been made by the Community and they have been credited.
I am only here to Compile and Provide backups of said data as my contribution back

Friendly Discord Servers (one way love from me so far) 
Name  | Invite
------ | ------
FreeMediaHeckyeah   |  https://discord.gg/fmhy
Esoterica Avenue   | https://discord.gg/enMG8bXUbn
BioHazard   | https://discord.gg/biohazard-official

Friendly SubReddits (one way love from me so far) 
Name  | Invite
------ | ------
FreeMediaHeckyeah   | https://www.reddit.com/r/FREEMEDIAHECKYEAH
Piracy   | https://www.reddit.com/r/Piracy/
SoftwarePirates   | https://www.reddit.com/r/SoftwarePirates/

## Getting Started
Name  | Link
------ | ------
Getting Started   |  https://rentry.co/MR-GetStarted
Sites to use and Avoid  | https://rentry.co/MR-Sites
Sites to use and Avoid (From Esoterica Avenue)   | https://rentry.org/EsotericaSiteList
Extensions & Scripts   | https://rentry.co/MR-Extensions-Script
Info About the Scene  | https://rentry.co/MR-SceneInfo


## Mega Threads (for various links)
Linked Directly to originals, No Edits, No Rehosts by me
Name  | Link
------ | ------
r/PiratedGames   | https://rentry.org/pgames-mega-thread
r/SoftwarePirates   | https://rentry.org/SoftwarePirates-Megathread

## Guides for Cracking / Debloating (Esoterica Avenue)
**Info**
This is a backup of the guides on the Discord server "Esoterica Avenue" Made and managed by  Orius (Ori#4448) 
Invite - https://discord.gg/enMG8bXUbn
There have been some edits to the guides to add - 
- Backup links
- Alternative Methods (if any)


**Guides**
Name  | Link
------ | ------
IDM   |  https://rentry.co/EA-IDM
JDownloader   | https://rentry.co/EA-JDownloader
Kaspersky   | https://rentry.co/EA-kaspersky

(Maintained by me)
Note - Not all have been Mirrored, new links will be added as they are edited & mirrored. 

## Windows 10 Guides
Note - These are external Rentry Links
Name  | Link
------ | ------
Activate Windows 10   |  https://rentry.co/ActivateWin10
Windows + Office Install for Babies   | https://rentry.co/windows_for_retards
Friendly Windows Thread   | https://rentry.co/fwt

Mirrors Links for Above linked
Name  | Link
------ | ------
Activate Windows 10   |  https://rentry.co/MR-W10
Windows + Office Install for Babies   | https://rentry.co/MR-BabyGuideForMS
Friendly Windows Thread  |  https://rentry.co/MR-EverythingWindowsExplained

## Direct to Drive
Name  | Link
------ | ------
AIO to Drive  |  <YET TO MAKE GUIDE>
Get a TD | https://rentry.co/MR-GetDrive


Notebook Link - https://mega.nz/folder/3IBxnCDB#bNi00UW0a5Xanvkztg0Z9w
File is in the folder
(Compiled by Me)

Edit History - 
(1) Post
(2) Added Windows + Office Guides
(3) Added MegaThreads, Friendly SubReddits and Discord Servers + AIO To Drive Linked
(4) Getting Started Links
(5) Spellings + Added Get a TD + EA Sites to Use and Avoid List