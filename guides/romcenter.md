### ** Rom Center Markdown Edition 2021 **



#### **TABLE OF CONTENTS**

Use ctrl+f to get around.

- Ublock Origin Tab
- Retro Tab
- Popular Games Tab
- Nintendo Tab
> DS
> DS Download Play
> DSi
> DSiWare
> 3DS
> GameCube
> Wii
> WiiWare and VC
> Wii U
- Sony Tab
> PS1
> PS2
> PS3
> PSP
> PS Vita
> More Games / Updates / DLCs
- Sega Tab
> Dreamcast
> Saturn
> CD & Mega CD
- Microsoft Tab
> Xbox
> Xbox 360
> XBLA
> XBLG
> Xbox Classic JTAG
- PC Tab
> MS-DOS
> Windows 3.x
> 1990s to early 2000s
> GoodOldDownloads
- Other Tab
> Arcade
> Romsets
> Miscellaneous
- Reputable ROM Sites
- Bypass Tab

## **Ublock Origin Tab**

This is the Ublock Origin Tab, before proceeding to the links make sure that you have this downloaded. 
Below are the links for the Chrome and Firefox Webstores where one can download Ublock Origin for free.

| Ublock Origin | Links |
| ------ | ------ |
| Chrome Webstore for use on Chrome DESKTOP Browser | [Link](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en) |
| Firefox Webstore for use on Firefox DESKTOP Browser| [Link](https://addons.mozilla.org/en-US/android/addon/ublock-origin/) |

As for Android, first go to the playstore download Firefox, this will be the browser that you download ALL roms with, after that tap
on the addons button and select Ublock Origin, it will be first on the list as it's the most popular addon for Android Firefox. Add it to Firefox,
and you are done. 


## **Retro Tab**

For retro games released on cartridge:
This includes NES, SNES GB and GBA

| Retro Games | No-Intro Collection |
| ------ | ------ |
|NES, SNES GB and GBA, etc | https://archive.org/details/no-intro_romsets |

To view/download the No-Intro Retro collection on the internet archive you must sign up for an account, this account is free.

| English Translations for Retro Games | Archived by /u/TheStorageManager |
| ------ | ------ |
| | https://archive.org/details/@storage_manager |

## **Popular Games Tab**

Instructions for using .nkit.gcz files:

Works on Dolphin emulator but not on real hardware.
If using real hardware, use [this](https://archive.org/compress/NKitFullyLoaded2020429) to convert to ISO.

## Metroid

Metroid Games

- |**Internet Archive**| |
| ------ | ------ |
| Metroid Prime Trilogy (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart4/Metroid%20Prime%20Trilogy%20%28USA%29/Metroid%20Prime%20Trilogy%20%28USA%29.nkit.gcz) |
| Metroid Prime (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart1/Metroid%20Prime%20%28USA%29.nkit.gcz) |
| Metroid Prime 2 (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart1/Metroid%20Prime%202%20-%20Echoes%20%28USA%29.nkit.gcz) |
| Metroid Prime 3 (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart4/Metroid%20Prime%203%20-%20Corruption%20%28USA%29/Metroid%20Prime%203%20-%20Corruption%20%28USA%29.nkit.gcz) |



## Mario

Mario Games

- |**Internet Archive**| |
| ------ | ------ |
| Mario Party 7 (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart1/Mario%20Party%207%20%28USA%29%20%28Rev%201%29.nkit.gcz) |
| Mario Party 8 (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart4/Mario%20Party%208%20%28USA%29%20%28Rev%201%29/Mario%20Party%208%20%28USA%29%20%28Rev%201%29.nkit.gcz) |
| Mario Party 9 (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart4/Mario%20Party%209%20%28USA%2C%20Asia%29%20%28En%2CFr%2CEs%29/Mario%20Party%209%20%28USA%2C%20Asia%29%20%28En%2CFr%2CEs%29.nkit.gcz) |
| Mario Kart Double Dash (GC) (USA) |[Link](https://archive.org/download/GCRedumpNKitPart1/Mario%20Kart%20-%20Double%20Dash%21%21%20%28USA%29.nkit.gcz) |
| Super Paper Mario (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart7/Super%20Paper%20Mario%20%28USA%29/Super%20Paper%20Mario%20%28USA%29.nkit.gcz) |
| Super Mario Sunshine (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart2/Super%20Mario%20Sunshine%20%28USA%29.nkit.gcz) |
| Super Mario Galaxy 1 (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart7/Super%20Mario%20Galaxy%20%28USA%29%20%28En%2CFr%2CEs%29/Super%20Mario%20Galaxy%20%28USA%29%20%28En%2CFr%2CEs%29.nkit.gcz) |
| Super Mario Galaxy 2 (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart7/Super%20Mario%20Galaxy%202%20%28USA%29%20%28En%2CFr%2CEs%29/Super%20Mario%20Galaxy%202%20%28USA%29%20%28En%2CFr%2CEs%29.nkit.gcz) |
| New Super Mario Bros. Wii (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart5/New%20Super%20Mario%20Bros.%20Wii%20%28USA%29%20%28En%2CFr%2CEs%29%20%28Rev%201%29/New%20Super%20Mario%20Bros.%20Wii%20%28USA%29%20%28En%2CFr%2CEs%29%20%28Rev%201%29.nkit.gcz) |

## Fire Emblem

Fire Emblem Games

- |**Fire Emblem**| |
| ------ | ------ |
| Fire Emblem Path of Radiance (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart1/Fire%20Emblem%20-%20Path%20of%20Radiance%20%28USA%29.nkit.gcz) |
| Fire Emblem Radiant Dawn (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart3/Fire%20Emblem%20-%20Radiant%20Dawn%20%28USA%29/Fire%20Emblem%20-%20Radiant%20Dawn%20%28USA%29.nkit.gcz) |
| Fire Emblem Fates Special Edition CIA (USA) | [Link](https://drive.google.com/file/d/1cwUzoPGJUmmmMjMb4_B-apWyM-XULc05/view?usp=sharing) / [DLC](https://drive.google.com/file/d/1KzuIYhdt7WabzdEj3A6Rbktmcl54LBXE/view?usp=sharing) |
| Fire Emblem Fates Special Edition 3DS (USA)| [Link](https://archive.org/download/3ds-main-encrypted/Fire%20Emblem%20Fates%20-%20Special%20Edition%20%28USA%29.7z) / [DLC](https://drive.google.com/file/d/1KzuIYhdt7WabzdEj3A6Rbktmcl54LBXE/view?usp=sharing) |

## Pokemon

Pokemon Games

- |**Internet Archive**| |
| ------ | ------ |
| Pokemon Red (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GB/Pokemon%20-%20Red%20Version%20%28USA%2C%20Europe%29%20%28SGB%20Enhanced%29.zip) |
| Pokemon Green (Japan) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GB/Pocket%20Monsters%20-%20Midori%20%28Japan%29%20%28SGB%20Enhanced%29.zip) |
| Pokemon Blue (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GB/Pokemon%20-%20Blue%20Version%20%28USA%2C%20Europe%29%20%28SGB%20Enhanced%29.zip) |
| Pokemon Yellow (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GB/Pokemon%20-%20Yellow%20Version%20-%20Special%20Pikachu%20Edition%20%28USA%2C%20Europe%29%20%28CGB%2BSGB%20Enhanced%29.zip) |
| Pokemon Gold (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBC/Pokemon%20-%20Gold%20Version%20%28USA%2C%20Europe%29%20%28SGB%20Enhanced%29%20%28GB%20Compatible%29.zip) |
| Pokemon Silver (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBC/Pokemon%20-%20Silver%20Version%20%28USA%2C%20Europe%29%20%28SGB%20Enhanced%29%20%28GB%20Compatible%29.zip) |
| Pokemon Crystal (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBC/Pokemon%20-%20Crystal%20Version%20%28USA%29.zip) |
| Pokemon Ruby (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBA/Pokemon%20-%20Ruby%20Version%20%28USA%29.zip) |
| Pokemon Sapphire (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBA/Pokemon%20-%20Sapphire%20Version%20%28USA%29.zip) |
| Pokemon FireRed (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBA/Pokemon%20-%20FireRed%20Version%20%28USA%29.zip) |
| Pokemon LeafGreen (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBA/Pokemon%20-%20LeafGreen%20Version%20%28USA%29.zip) |
| Pokemon Emerald (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/GBA/Pokemon%20-%20Emerald%20Version%20%28USA%2C%20Europe%29.zip) |
| Pokemon Diamond (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20Diamond%20Version%20%28USA%29%20%28Rev%205%29.zip) |
| Pokemon Pearl (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20Pearl%20Version%20%28USA%29%20%28Rev%205%29.zip) |
| Pokemon Platinum (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20Platinum%20Version%20%28USA%29.zip) |
| Pokemon HeartGold (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20HeartGold%20Version%20%28USA%29.zip) |
| Pokemon SoulSilver (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20SoulSilver%20Version%20%28USA%29.zip) |
| Pokemon Black (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20Black%20Version%20%28USA%2C%20Europe%29%20%28NDSi%20Enhanced%29.zip) |
| Pokemon White (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20White%20Version%20%28USA%2C%20Europe%29%20%28NDSi%20Enhanced%29.zip) |
| Pokemon Black 2 (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20Black%20Version%202%20%28USA%2C%20Europe%29%20%28NDSi%20Enhanced%29.zip) |
| Pokemon White 2 (USA) | [Link](https://archive.org/download/pkmn_collection/pkmn%20collection/NDS/Pokemon%20-%20White%20Version%202%20%28USA%2C%20Europe%29%20%28NDSi%20Enhanced%29.zip) |
| Pokemon X (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20X%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20X%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%28CIA%29.7z) |
| Pokemon Y (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Y%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Y%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%28CIA%29.7z) |
| Pokemon Omega Ruby (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Omega%20Ruby%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%28Rev%202%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Omega%20Ruby%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%28Rev%202%29%20%28CIA%29.7z) |
| Pokemon Alpha Sapphire (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Alpha%20Sapphire%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%28Rev%202%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Alpha%20Sapphire%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CKo%29%20%28Rev%202%29%20%28CIA%29.7z) |
| Pokemon Sun (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Sun%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Sun%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%28CIA%29.7z) | 
| Pokemon Moon (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Moon%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Moon%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%28CIA%29.7z) |
| Pokemon Ultra Sun (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Ultra%20Sun%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Ultra%20Sun%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%28CIA%29.7z) |
| Pokemon Ultra Moon (USA) | [.3ds](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Ultra%20Moon%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%283DS%29.7z) / [.cia](https://archive.org/download/pkmn_collection/3DS/Pokemon%20Ultra%20Moon%20%28USA%29%20%28En%2CJa%2CFr%2CDe%2CEs%2CIt%2CZh%2CKo%29%20%28CIA%29.7z) |

## Persona

Persona Games

- |**Persona**| |
| ------ | ------ |
| Persona 5 (USA) | [Link](https://nopaystation.com/view/PS3/NPUB31848/HDDBOOTPERSONA05/1?version=1) |
| Persona 4 (USA) | [Link](https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01Part2/Shin%20Megami%20Tensei%20-%20Persona%204%20%28USA%29.7z) |
| Persona 4 Golden (USA) | [Vita](https://nopaystation.com/view/PSV/PCSE00120/PERSONA4GOLDEN01/1?version=1.01) / [PC](https://drive.google.com/file/d/1dALfgu038IyOiT2rCd4iK1tfy7Q55DBO/view?usp=sharing) |
| Persona 3 (USA) | [Link](https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01Part2/Shin%20Megami%20Tensei%20-%20Persona%203%20%28USA%29.7z) |
| Persona 3 FES (USA) | [Link](https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01Part2/Shin%20Megami%20Tensei%20-%20Persona%203%20FES%20%28USA%29.7z) |
| Persona 3 Portable (USA) | [Link](https://archive.org/download/redump.psp.p2/Shin%20Megami%20Tensei%20-%20Persona%203%20Portable%20%28USA%29.zip) |
| Persona 2: Innocent Sin (USA/Japan) | [PSP](https://archive.org/download/redump.psp.p2/Shin%20Megami%20Tensei%20-%20Persona%202%20-%20Innocent%20Sin%20%28USA%29.zip) / [PS1](https://archive.org/download/redump.psx.p3/Persona%202%20-%20Tsumi%20-%20Innocent%20Sin%20%28Japan%29.zip) |
| Persona 2: Eternal Punishment (USA) | [Link](https://archive.org/download/redump.psx.p3/Persona%202%20-%20Eternal%20Punishment%20%28USA%29.zip) |
| Persona 1 (USA) | [PSP](https://archive.org/download/redump.psp.p2/Shin%20Megami%20Tensei%20-%20Persona%20%28USA%29.zip) / [PS1](https://archive.org/download/redump.psx.p3/Persona%20%28USA%29.zip) |

## Zelda

Zelda Games

- |**Legend of Zelda**| |
| ------ | ------ |
| Legend of Zelda Breath of the Wild (Wii U) (USA) | [Game](https://drive.google.com/file/d/1CckvkzzQ8onTOcceKX_HIC9reu9RYw5I/view?usp=sharing) / [DLC](https://drive.google.com/file/d/1vtE7EMMRw3WggWb2YmcC0kdYbNEbjjmL/view?usp=sharing) / [Update](https://drive.google.com/file/d/1_a8omXQOJ2s1M8Ae2YPWKBIA5aF8CzgK/view?usp=sharing) |
| Legend of Zelda Twilight Princess (Wii) (USA) | [Link](https://archive.org/download/WiiRedumpNKitPart4/Legend%20of%20Zelda%2C%20The%20-%20Twilight%20Princess%20%28USA%29/Legend%20of%20Zelda%2C%20The%20-%20Twilight%20Princess%20%28USA%29.nkit.gcz) |
| Legend of Zelda Twilight Princess (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart1/Legend%20of%20Zelda%2C%20The%20-%20Twilight%20Princess%20%28USA%29.nkit.gcz) |
| Legend of Zelda Skyward Sword (Wii) (USA)| [Link](https://archive.org/download/WiiRedumpNKitPart4/Legend%20of%20Zelda%2C%20The%20-%20Skyward%20Sword%20%28USA%29%20%28En%2CFr%2CEs%29/Legend%20of%20Zelda%2C%20The%20-%20Skyward%20Sword%20%28USA%29%20%28En%2CFr%2CEs%29.nkit.gcz) |
| Legend of Zelda Wind Waker (GC) (USA) | [Link](https://archive.org/download/GCRedumpNKitPart1/Legend%20of%20Zelda%2C%20The%20-%20The%20Wind%20Waker%20%28USA%29.nkit.gcz) |
| Legend of Zelda Ocarina of Time| [.cia](https://drive.google.com/file/d/1-sf2HaW4h-II0ZKK8nskSLDxT5ewgSJX/view?usp=sharing) / [.3ds](https://archive.org/download/nintendo-3ds-complete-collection/3DS0033%20-%20The%20Legend%20of%20Zelda%20Ocarina%20of%20Time%203D%20%28U%29.3ds.7z) |
| Legend of Zelda Ocarina of Time (N64) (USA) | [Link](https://archive.org/download/pkmn_collection/Legend%20of%20Zelda%2C%20The%20-%20Ocarina%20of%20Time%20%28USA%29.zip) |

## Super Smash Bros

Super Smash Bros Games

- |**Super Smash Bros**| | 
| ------ | ------ |
| Super Smash Bros Wii U | [Game](https://drive.google.com/file/d/1lbLLPYJQM3Y5oRoB74XLsFOVLbXFFgs-/view?usp=sharing) / [DLC](https://drive.google.com/file/d/1Jh6CNafyvrTFkS-36UVCEMDn0xKn4_SX/view?usp=sharing) / [Update](https://drive.google.com/file/d/1mXwmcZf6Z-tRm8Dmkb7tjbz_qZFx5YHf/view?usp=sharing) |
| Super Smash Bros Brawl ISO (Wii)| [Link](https://archive.org/download/WiiRedumpNKitPart7/Super%20Smash%20Bros.%20Brawl%20%28USA%29%20%28Rev%201%29/Super%20Smash%20Bros.%20Brawl%20%28USA%29%20%28Rev%201%29.nkit.gcz) |
| Super Smash Bros Melee v1.02 ISO (GC)| [Link](https://archive.org/download/GCRedumpNKitPart2/Super%20Smash%20Bros.%20Melee%20%28USA%29%20%28En%2CJa%29.nkit.gcz) | 

## Xenoblade

Xenoblade Games

- |**Xenoblade**| |
| ------ | ------ |
| Xenoblade Chronicles (Wii) (USA)| [Link](https://archive.org/download/WiiRedumpNKitPart8/Xenoblade%20Chronicles%20%28USA%29%20%28En%2CFr%2CEs%29/Xenoblade%20Chronicles%20%28USA%29%20%28En%2CFr%2CEs%29.nkit.gcz) |
| Xenoblade Chronicles X (Wii U) (USA) | [Game](https://drive.google.com/file/d/1_GYMvb3y54sYIHJhUyGNt9S8zIiFB2kc/view?usp=sharing) / [DLC](https://drive.google.com/file/d/1eXrKAlaQZMghVAv_Q6zj-akvFqS9OT8x/view?usp=sharing) / [Update](https://drive.google.com/file/d/1i-9Lzbq5vakIlvt6oGWquewGFWa9hke6/view?usp=sharing) |

## **Nintendo Tab**

|DS | |
| ------ | ------ |
|Internet Archive|https://archive.org/download/noIntroNintendoDsDecrypted2019Jun30|
| Mobasuitecom No-Intro 2021 | [Link](http://90.230.15.92/Nintendo/Nintendo%20DS/) |
| Alvro Rebranded Mirror | [Link](https://drive.google.com/drive/folders/1s3zKz90Nb-5L3LemxaCTcc5V9fZ4z00o) |

|DS Download Play| |
| ------ | ------ |
|Internet Archive No-Intro 2019 | https://archive.org/download/nintendodsdownloadplay | 

|DSi| |
| ------ | ------ |
|Internet Archive| https://archive.org/download/nintendodsidecrypted|

|DSiWare| |
| ------ | ------ |
|Internet Archive| https://archive.org/download/nintendodsiware|

|3DS| |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
|No-Intro 3DS| https://drive.google.com/drive/folders/1R5c6-nY5mMns8G1u2tcbYumfTCGXDQ5w|

- |**Internet Archive: ALL **| |
| ------ | ------ |
|No-Intro Nintendo 3DS Encrypted .3ds Part 1| https://archive.org/details/3ds-main-encrypted|
|No-Intro Nintendo 3DS Encrypted .3ds Part 2| https://archive.org/details/3ds-main-encrypted-p2|

- |**Mega: ALL **| |
| ------ | ------ |
|Mega| https://drive.google.com/file/d/1r1CS6UVPLafPbCFM7sjF1baupsEr5APH/view|
This contains megalinks, read the top of the document for instructions

- |**Google Drive: ALL **| |
| ------ | ------ |
| 3DS ROMs Scene No RAR | [Link](https://drive.google.com/drive/folders/1Gd5cJUstN6gwZI9lKzHcDWJ-kh0ktexL) |
| 3DS eShop Scene No RAR | [Link](https://drive.google.com/drive/folders/1MDy65pGaoIOmoIg__fey3b_CjKNm3Ujc) |
| 3DS x-files | [Link](https://drive.google.com/drive/folders/1BLaIl4v3cgmdpoXx8bB7q33Vvh_mt32n) |
| 3DS CIA Scene No RAR| [Link](https://drive.google.com/drive/folders/1FWWm3qNg_HHANTmEeY_SxRRabUm_3wgC) |
| 3DS CIA eShop Scene No RAR | [Link](https://drive.google.com/drive/folders/1Y8yGQ9-tTWdleCbTMiEI4-XiFOYLpcNs) |
| 3DS CIA DLC | [Link](https://drive.google.com/drive/folders/1ARm3uFESLi9r1pbym_XR5lNbDb8_BUDS) |
| 3DS CIA Updates | [Link](https://drive.google.com/drive/folders/17ltCPytHV4qSftGPvFwxGBfmfiwJfjjU) |
| 3DS Legit CIA All | [Link](https://drive.google.com/drive/folders/1G7o0kFXpT4Qzwehe_rrs_zWqn45nCkYV) |
| 3DS Decrypted Citra | [Link](https://drive.google.com/drive/folders/1JPW_yrSrqF_hnXtlpR-6-qXUitEAeQXz) |
| 3DS Decrypted Citra eShop | [Link](https://drive.google.com/drive/folders/1BHCXsJYQvqsalW5IaU4N9bkCdhU1fILB)|
| 3DS No-Intro | [Link](https://drive.google.com/drive/folders/1oImDLb5j8FtXnrQy9VVSRVOYVutJVbI4) |
| 3DS No-Intro eShop | [Link](https://drive.google.com/drive/folders/1u8Ro8KtCPPAy6eOIZ7KlFF8u-LJEj7l0) |
| N3DS No-Intro | [Link](https://drive.google.com/drive/folders/1hRsLubAne0hFHxJ9K7eJORc4n0pCCDFG) |
| N3DS No-Intro eShop | [Link](https://drive.google.com/drive/folders/1siyQFUEqqtc9z-SBtqAjvxjfi2zmx0sy) |
| 3DS .cia No-Intro | [Link](https://drive.google.com/drive/folders/1igGdcIznoZiloF8RK0Jwbn2YZI5nztyo) |
| 3DS .cia No-Intro eShop | [Link](https://drive.google.com/drive/folders/1hWKO_6v7ud_WXB9RxDB2MuwLqvuPmFhc) |
| N3DS .cia No-Intro | [Link](https://drive.google.com/drive/folders/1GW2MQxIol-HlOn4eN4NZZTXttnsi54pQ) |

- |**.3ds/.cia Decryption Instructions**| |
| ------ | ------ |

If Citra says the roms are encrypted, use either download https://gbatemp.net/download/batch-cia-3ds-decryptor.35098/ to decrypt.

Or add the following AES keys to CitraAES Keys for Citra, https://pastebin.com/tBY6RHh4
Click download on Pastebin to download the text in this file as a .txt file.

Windows installation instructions
Add the downloaded pastebin text file to C:Users"your_user_name"AppDataRoamingCitrasysdata make sure it is named aes_keys.txt

Linux/MacOS Installation instructions
Add the downloaded pastebin text file to ~/.local/share/citra-emu/sysdata make sure it is named aes_keys.txt

Read this for more information, https://citra-emu.org/wiki/user-directory/

|Gamecube| |
| ------ | ------ |

- |**NKit Format**| |
| ------ | ------ |
|Internet Archive (NKit) Redump Part 1 |https://archive.org/download/GCRedumpNKitPart1|
|Internet Archive (NKit) Redump Part 2 |https://archive.org/download/GCRedumpNKitPart2|

- |**ISO Format**| |
| ------ | ------ |
|Internet Archive (ISO) Redump USA Part 1 | https://archive.org/download/RedumpNintendoGameCubeAmerica| 
| Internet Archive (ISO) Redump USA Part 2| https://archive.org/download/RedumpNintendoGameCubeAmericaPart2| 
| Internet Archive (ISO) Redump USA Part 3| https://archive.org/download/RedumpNintendoGameCubeAmericaPart3|
|Internet Archive (ISO) Japan| https://archive.org/download/NCubeJ| 
| Internet Archive (ISO) USA| https://archive.org/download/GamecubeCollectionByGhostware| 
| Internet Archive (ISO) Europe| https://archive.org/download/EuropeanGamecubeCollectionByGhostware| 
| Internet Archive (ISO) Oceania| https://archive.org/download/AustraliaGamecubeCollectionByGhostware| 
| Internet Archive (ISO) Asia| https://archive.org/download/AsiaGamecubeCollectionByGhostware| 
| RapidGator All regions, Alvro Collection| https://rapidgator.net/folder/5842373/Gamecube.html| 

|Wii| |
| ------ | ------ |

- |**NKit Format**| |
| ------ | ------ |
|Internet Archive (NKit) Redump Part 1 |https://archive.org/download/WiiRedumpNKitPart1|
|Internet Archive (NKit) Redump Part 2 |https://archive.org/download/WiiRedumpNKitPart2|
|Internet Archive (NKit) Redump Part 3 |https://archive.org/download/WiiRedumpNKitPart3|
|Internet Archive (NKit) Redump Part 4 |https://archive.org/download/WiiRedumpNKitPart4|
|Internet Archive (NKit) Redump Part 5 |https://archive.org/download/WiiRedumpNKitPart5|
|Internet Archive (NKit) Redump Part 6 |https://archive.org/download/WiiRedumpNKitPart6|
|Internet Archive (NKit) Redump Part 7 |https://archive.org/download/WiiRedumpNKitPart7|
|Internet Archive (NKit) Redump Part 8 |https://archive.org/download/WiiRedumpNKitPart8|

Works on Dolphin emulator but not on real hardware.
If using real hardware, use https://vimm.net/vault/?p=nkit to convert to ISO.

- |**nNASOS Format**| |
| ------ | ------ |
|Internet Archive (nNASOS) Redump USA Part 1| https://archive.org/download/RedumpNintendoWiiAmericaPart1|
|Internet Archive (nNASOS) Redump USA Part 2| https://archive.org/download/RedumpNintendoWiiAmericaPart2|
|Internet Archive (nNASOS) Redump USA Part 3| https://archive.org/download/RedumpNintendoWiiAmericaPart3|
|Internet Archive (nNASOS) Redump USA Part 4| https://archive.org/download/RedumpNintendoWiiAmericaPart3_201802|
|Internet Archive (nNASOS) Redump USA Part 5| https://archive.org/download/RedumpNintendoWiiAmericaPart5|
|Internet Archive (nNASOS) Redump USA Part 5| https://archive.org/download/Httpsarchive.orgdetailsRedumpNintendoWiiAmericaPart6|

To extract from .dec files:
Download https://archive.org/download/nNASOS1.8/nNASOS1.8.zip
and extract the files into a folder.
To extract a rom drag the rom onto the .exe.
A command prompt should popup and extract the ISO from the .dec file.

|Wiiware and VC| |
| ------ | ------ |
|Google Drive| https://drive.google.com/drive/folders/1ZpX5Nh4BNzWDvpXJJuqVlX2Qz7xpwpVU|

|Wii U| |
| ------ | ------ |
|Tool| https://www.reddit.com/r/CemuPiracy/comments/bszm0p/how_to_set_up_wii_u_usb_helper_after_its/|

Wii U USB Helper, Check the checkbox that says "Unpack" to use the files with cemu emulator.

- |**Scene Games**| |
| ------ | ------ |
| Alvro Rebranded Mirror, Wii U Scene Games | [Link](https://drive.google.com/drive/folders/1k6bhO9VhZWdq7Lz6fY6RxOrs9D8lm2ks) |

- |**NUS Format**| |
| ------ | ------ |
| Wii U NUS Disc | [Link](https://drive.google.com/drive/folders/1l5yBGCynkNLeC0CbSZKoSiFmwd7_mTVt) |
| Wii U NUS Download All Regions | [Link](https://drive.google.com/drive/folders/1NCiboZf3uTD3sVPkTN288s-mpbRkgYwX) |
| Wii U NUS Retail All Regions | [Link](https://drive.google.com/drive/folders/1gG4_3uQAQaWJrg9LC4KjcYFsfxI4xb17) |
| Wii U NUS Virtual Console | [Link](https://drive.google.com/drive/folders/14WAI96ztCoJtm-XsAsN6S0qL5XdDHDwV) |

- |**RAW Format**| |
| ------ | ------ |
| Wii U RAW Disc | [Link](https://drive.google.com/drive/folders/1B9h4tg0GTP1MoWdhCq2jhTsU7UsTOkA5) |
| Wii U RAW Download All Regions | [Link](https://drive.google.com/drive/folders/1Zo8xAsmHJehtMJ8dK3AFIP8lA1nHcVLT) |
| Wii U RAW Retail All Regions | [Link](https://drive.google.com/drive/folders/1uvkvNEbSzOb6xQhxIUjpm0d7M-ewLCv4) |
| Wii U RAW Virtual Console | [Link](https://drive.google.com/drive/folders/1mjHeJV_eW3lqIEWW7-2KLy7iU8AJH3uE) |

## **Sony Tab**

|PS1 | |
| ------ | ------ |

- |**Internet Archive: ALL **| |
| ------ | ------ |
|PS1 Redump USA |https://archive.org/download/Sony-Playstation-USA-Redump.org-2019-05-27|
| PS1 Redump Europe Part 1| https://archive.org/download/Sony-Playstation-EUR-Redump.org|
| PS1 Redump Europe Part 2| https://archive.org/download/SonyPlaystation-EUR-Part2-Redump.org2019-06-05|
| PS1 Redump Asia Part 1| https://archive.org/download/Sony-Playstation-JP-ASIA-Part-1|
| PS1 Redump Asia Part 2| https://archive.org/download/Sony-Playstation-JP-ASIA-Part-2|
| PS1 Redump .CUE/.BIN Part 1 | https://archive.org/download/redump.psx|
| PS1 Redump .CUE/.BIN Part 2 | https://archive.org/download/redump.psx.p2|
| PS1 Redump .CUE/.BIN Part 3 | https://archive.org/download/redump.psx.p3|
| PS1 Redump .CUE/.BIN Part 4 | https://archive.org/download/redump.psx.p4|
| PS1 Redump .CHD USA | https://archive.org/download/chd_psx/CHD-PSX-USA/|
| PS1 Redump .CHD EUR | https://archive.org/download/chd_psx_eur/CHD-PSX-EUR/|
| PS1 Redump .CHD JPN | https://archive.org/download/chd_psx_jap/CHD-PSX-JAP/|
| PS1 Redump .CHD Misc | https://archive.org/download/chd_psx_misc/CHD-PSX-Misc/|

|PS2 | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
|Europe| [Link](https://drive.google.com/drive/folders/19lQ2WXShHBWNnuL34EPgugNTGVIW9OPt) |
|USA| [Link](https://drive.google.com/drive/folders/1tE8uKfEVA3t4Ic7a2cfC28COtRQlo7X8) |
|Japanese| [Link](https://drive.google.com/drive/folders/1HdKHkKKXd494fuXsFKACs_VENI1JG7DN) |

- |**Internet Archive: USA **| |
| ------ | ------ |
| PS2 Redump USA Part 1|https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01
| PS2 Redump USA Part 2| https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01Part2
| PS2 Redump USA Part 3| https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01Part3
| PS2 Redump USA Part 4| https://archive.org/download/redumpSonyPlaystation2UsaGames2018Aug01Part4
| PS2 Redump USA Part 5| https://archive.org/download/redumpSonyPlaystation2UsaOther2018Aug01

|PS3 | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
| PS3 ISO | [Link](https://drive.google.com/drive/folders/1DlsZIGn-EtRzgoPPNVMTFsXH-ykXJOcx) |
| PSN | [Link](https://drive.google.com/drive/folders/1ako0uO8UCyFPOf25jFFTXWJw4_1Yq49c) |
| PSN No-Intro All Regions | [Link](https://drive.google.com/drive/folders/1Rmp7wIZ-MrBgPxAu48t07_3z7ICzDhht) |

|PSP | |
| ------ | ------ |

- |**Internet Archive: ALL **| |
| ------ | ------ |
| PSP ISO Part 1 | https://archive.org/download/redump.psp|
| PSP ISO Part 2 | https://archive.org/download/redump.psp.p2|

- |**Google Drive: ALL **| |
| ------ | ------ |
| Games by number | [Link](https://drive.google.com/drive/folders/1bnwkRPz0Kbdj-tEsBXsoeNc6ZjF1GwU2) |
| Games by letter | [Link](https://drive.google.com/drive/folders/15feAVYtmtF4A9qEylsD0536ReX7PQYGl) |
| UMD Music | [Link](https://drive.google.com/drive/folders/1GtTHVUeeB0xQNkcq3V30vNFby24YpHfe) |
| PSX2PSP | [Link](https://drive.google.com/drive/folders/1ExPkcPbIC9bMZAcya5c72vn_flk6OBU6) |
| PSN | [Link](https://drive.google.com/drive/folders/1NtVUC0L7CvbNZP2s5yutuye9LstKf6mS) |
| PSN DLC | [Link](https://drive.google.com/drive/folders/1xKmpV5dglj0W8cLH4GHXeE5BhLLv_DtT) |

|PS Vita | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
| VPK | [Link](https://drive.google.com/drive/folders/1Ju3RuYDUqV54UcvyMRuoiGUH-dkfj_9j) |
| Patched | [Link](https://drive.google.com/drive/folders/1SMtzuE0EaRqjuunVIoUGQnkhYS4BoRmO) |
| VPK No-Intro | [Link](https://drive.google.com/drive/folders/175-iMS6cwcLNd7l1JF1O_B8ikBKmAqZ0) |
| PSN | [Link](https://drive.google.com/drive/folders/1S96pjVr-KLsmlb6m7bcHcgl4glp90aSz) |
| MaiDump | [Link](https://drive.google.com/drive/folders/15xhmueFKMyQ6Oi8FnT2eDe87C5Ksgvyh) |

|For More Games/Updates/DLCs| |
| ------ | ------ |
|NoPayStation| https://docs.google.com/spreadsheets/d/18HHxaQhGgqDjH2mIgC3T1OVWNRr_4HHfuLsYmiKXnfk/edit#gid=597019973|

## **Sega Tab**

|Dreamcast | |
| ------ | ------ |

- |**Internet Archive: ALL **| |
| ------ | ------ |
| Redump Sega Dreamcast| https://archive.org/download/RedumpSegaDreamcast20160613
| Redump Sega Dreamcast .CUE/.BIN | https://archive.org/download/redump.dc.revival|
| Redump Sega Dreamcast .CHD | https://archive.org/download/chd_dc/CHD-Dreamcast/|
| Dreamcast Uploaded by Ghostware | https://archive.org/download/DreamcastCollectionByGhostwareMulti-region
| Almost Complete Dreamcast| https://archive.org/download/almstcmpltdrmcst
| TOSEC Dreamcast | https://archive.org/download/tosecdcus20190822
GDIs

- |**Google Drive: ALL **| |
| ------ | ------ |
|Alvro Rebranded Mirror |[Link](https://drive.google.com/drive/folders/1gJvfa6RbFTgRJeMxhDVqRkSa86Pzm_OP) |

Note: The zips in the 1fichier link are in bin/cue format.
To play dreamcast on emulators, please use sets with GDIs.

- |**Google Drive: ALL **| |
| ------ | ------ |
| CDI Collection | https://docs.google.com/spreadsheets/d/14fCQ3NXIlW1ZC_gjIejpQVPG34fLWmSoXYgSTyxdRWM/edit#gid=0|
| TiZ Homebrew Games | https://drive.google.com/drive/folders/1qIlLS51uBLEmsguvxNTWj8Qg2BAyF1dE|
| TiZ Homebrew Apps | https://mega.nz/#F!q7oxzDga!JfJulP8EX1-poB0nkgy2ZA|

|Saturn | |
| ------ | ------ |

- |**Internet Archive: ALL **| |
| ------ | ------ |
| Redump Sega Saturn .CUE/.BIN | https://archive.org/download/redump.ss.revival|
| Redump Sega Saturn .CHD USA | https://archive.org/download/chd_saturn/CHD-Saturn/USA/|
| Redump Sega Saturn .CHD EUR | https://archive.org/download/chd_saturn/CHD-Saturn/Europe/|
| Redump Sega Saturn .CHD JPN | https://archive.org/download/chd_saturn/CHD-Saturn/Japan/| 
| Redump Sega Saturn .CHD Other | https://archive.org/download/chd_saturn/CHD-Saturn/Other-Regions/| 

- |**Google Drive: ALL **| |
| ------ | ------ |
|Sega Saturn ISO Europe | [Link](https://drive.google.com/drive/folders/11FRcw9NoM6X3mM4qm2sqvVEw-N763oGH) |
|Sega Saturn ISO Japan | [Link](https://drive.google.com/drive/folders/1R61XOnfZg3rzXvKwtGn7ngfo_Hqn3p7s) |
|Sega Saturn ISO USA | [Link](https://drive.google.com/drive/folders/11e69IUrrKJ_wTGSdGUuDXB4LLuG2JYHv) |

|CD & Mega CD | |
| ------ | ------ |

- |**Internet Archive: ALL **| |
| ------ | ------ |
| Redump Sega Mega CD .CUE/.BIN | https://archive.org/download/redump.mcd.revival|
| Redump Sega CD & Mega CD .CHD USA | https://archive.org/download/chd_segacd/CHD-SegaCD-NTSC/|
| Redump Sega CD & Mega CD .CHD EUR | https://archive.org/download/chd_segacd/CHD-MegaCD-PAL/|
| Redump Sega CD & Mega CD .CHD JPN | https://archive.org/download/chd_segacd/CHD-MegaCD-NTSCJ/|

- |**Google Drive: ALL **| |
| ------ | ------ |
|Sega Mega CD | [Link](https://drive.google.com/drive/folders/1oGayIffep4QioAjV2hTyBaEpa_VqUfU4) |
|Sega Mega CD Redump | [Link](https://drive.google.com/drive/folders/1RuHTIyg6V4f2V-X5rYLsWRXOj-Nla6y-) |

## **Microsoft Tab**

|Xbox | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
| Xbox | [Link](https://drive.google.com/drive/folders/1m7Zf72XX4M2nQjI4zGRw8aZPtQqm79YP) |

- |**Internet Archive: Multiple **| |
| ------ | ------ |
| Xbox HDD Ready Part 1 |https://archive.org/details/XBOX_HDD_READY|
| Xbox HDD Ready Part 2 | https://archive.org/details/XBOX_HDD_READY_2|
| Xbox HDD Ready Part 3 | https://archive.org/details/XBOX_HDD_READY_2_201710|
| Xbox HDD Ready Part 4 | https://archive.org/details/XBOX_HDD_READY_3|

|Xbox 360 | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
| ISO | [Link](https://drive.google.com/drive/folders/1vTk6alNUwMCo5RarcOiyHvPFjdTlTfwu) |
| DLC | [Link](https://drive.google.com/drive/folders/1YpVTuyNb3xoFz4q7JGsWzXm1fGcFCzGD) |

|Xbox Live Arcade | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
|XBLA| [Link](https://drive.google.com/drive/folders/1YOuaQHFW_Ic_sQg1--FvyBbmqxLXrJRj) |
|XBLA DLC| [Link](https://drive.google.com/drive/folders/1IkhUA6YGZeL1uZjCdeG5JVmRXP684VKe) |

|Xbox Live Indie Games | |
| ------ | ------ |

- |**Google Drive: ALL **| |
| ------ | ------ |
| XBLIG | [Link](https://drive.google.com/drive/folders/1eJz3kYUSSkvjUBs88LZZxT4bm-E_8w3L) |
| XNA Title Update.rar| [Link](https://drive.google.com/drive/folders/1jAPoCui1f4hysh85nB6OAwkN9IH-z0ed) |

|Xbox Classic JTAG | |
| ------ | ------ |

- |**1fichier: ALL **| |
| ------ | ------ |
| XBOX CLASSIC ISO | [Link](https://drive.google.com/drive/folders/1utNRA6jbt19UCMGLYHo7qg6d6jJuoVhJ) |
| XDVD MULLETER + DMI + PFI + SS | [Link](https://drive.google.com/drive/folders/1VFpqp_rC5SivnjNc0EhOLxXAuYIK8oO2) |
| Xbox CLASSIC JTAG | [Link](https://drive.google.com/drive/folders/1qWdADm1eYd7ADjvON0N9MkzPYn8WfmFL) |
| Xbox 360 Backwards Compatibility | [Link](https://drive.google.com/drive/folders/1raK5FRJsWSWR5YfueV_aed33J97K3SDU) |

## **PC Tab**

|MS-DOS | |
| ------ | ------ |
|Total DOS Collection #16 | https://archive.org/download/Total_DOS_Collection_Release_16_March_2019|
|eXoDOS v5 | https://exodos.the-eye.eu/|

|Windows 3.x | |
| ------ | ------ |
|Win3xO | | https://the-eye.eu/public/Games/eXo/Win3xO/|

|1990s to early 2000s | |
| ------ | ------ |
|Internet Archive | https://archive.org/|
|MyAbandonware | https://www.myabandonware.com/

|GoodOldDownloads | |
| ------ | ------ |
|GoodOldDownloads | https://gog-games.com|

- |**Self-hosted **| |
| ------ | ------ |
| Mirror | https://goggamespc7v6z5e.onion.ws/
| Mirror| https://freegogpcgames.com/
| Mirror| http://www.ovagames.com/

## **Other Tab**

|Arcade | |
| ------ | ------ |
|MAME 0.202 ROMs Zipped | https://1fichier.com/?eoktp9g9skrzsed496zx|
|MAME 0.220 ROMs Software List | https://archive.org/download/mame_sl_0220/MAME%20SL%200.220%20(Merged)/|
|MAME 0.229 CHDs | [Link](https://archive.org/download/MAME_0.225_CHDs_merged)|
|MAME 0.229 ROMS | [Link](https://archive.org/download/mame.0229) |
|Final Burn Neo 1.0.0.0 ROMs| https://archive.org/download/fbneo/FBNeo/|
|Final Burn Neo | https://archive.org/download/2020_01_06_fbn|

|Romsets | |
| ------ | ------ |
|No-Intro 2021 | https://archive.org/download/no-intro_romsets/no-intro%20romsets/|
|GoodSets | https://1fichier.com/dir/ugDwQd8N|
|TOSEC 2018-12-27 | https://archive.org/download/TOSEC_Main_Branch_Release_2018-12-27|
|EverDrive 2018 Torrent | https://pastebin.com/raw/ywTQeDmS|

|Miscellaneous | |
| ------ | ------ |
|Doom WADs | https://archive.org/download/2020_03_22_DOOM/DOOM%20WADs/|
|RetroArch BIOS| https://archive.org/download/RetroarchSystemFiles/Retroarch-System/|
 
|Reputable ROM Sites and Collections | |
| ------ | ------ |

edge|emulation has GoodSets, and a decent amount of ROMs.

Vimm's Lair Vimm is cool and Vimm's Lair is easily the oldest rom site on Earth. Good collection of ROMs

Mobasuitecom's Rom's server http://90.230.15.92/ Made by Mobasuitecom also known as Loggan

Alvro Rebranded Mirror Google Drive https://drive.google.com/drive/folders/1rEtYPc8XTTrfye-dNXuhwBqnny33ZLeB

## **Bypass Tab**

|GDrive Quota Bypass Method | |
| ------ | ------ |

From time to time, when trying to download a file, you might see an error message saying “Download quota exceeded, so you can’t download this file at this time”. Here’s how to bypass that message and download what you want:

1. Log into your Google account, if you haven’t already.
2. Go to Google Drive, and click “My Drive” in the sidebar.
3. Make a new folder in your drive, it doesn’t matter what you name it.
4. Go to the quota'd folder, and right click on whatever you want to download and press “Add Shortcut to Drive”.
5. Navigate to “My Drive” and then to the folder you made, then press “Add Shortcut”.
6. Go to “My Drive” on the sidebar, then right click the folder you made and press “Download”.
7. A zip file will be made with the file(s) you selected. Enjoy!