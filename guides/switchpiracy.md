# The Ultimate Guide to Nintendo Switch

-   [Getting Started](#getting-started)
    -   [Is my Switch hackable?](#is-my-switch-hackable)
    -   [How to avoid getting banned](#avoid-getting-banned)
    -   [How to launch payloads](#launching=payloads)
    -   [Create/Restore a NAND backup](#nand-backups)
    -   [Dumping device keys](#dumping-device-keys)


### Is my Switch hackable?
***
All current Switch models are hackable, however the exploit methods are different and vary in difficulty. 
This guide will help you identify what Switch model you own and what exploit you can use. 

#### Current Switch models:
***
**Erista (Unpatched):**
* If you purchased your switch before august 2018, then you have an unpatched Erista model and your Switch can use the **RCM** vulnerability. 

**Erista (IPatched):**
* If you purchased your Switch after august 2018, it *may* be patched. You can still check if your Switch is unpatched [here](https://ismyswitchpatched.com/).
	* If your Switch is unpatched, then your Switch can use the **RCM** vulnerability.
	* If your Switch is patched and is on firmware `4.1.0` then you can use **PegaScape** to hack your Switch.
	* If your Switch is patched and is on a later firmware version then you can use an **SX Core** or a **HWFLY** modchip to modify your Switch.
        - *Please be aware that **SX Core** modchips are extremely hard to find due to being discontinued and require to be soldered onto the motherboard.*
        - ***HWFLY** modchips are easier to find , however they are extremely expensive, ranging from 190$ ~ $270 and also require to be soldered onto the motherboard.*

**Mariko:**
* If you have a Switch that came in a [red box](https://cdn.vox-cdn.com/thumbor/3F8t1_KuVvJeTnF-NmkyW_oWvDM=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/18963723/switchv2.jpg), a Switch Lite or an OLED Switch, then it's a Mariko model. 
Mariko cannot use the **RCM** vulnerability. However you can use an **SX Core**, **SX Lite**, or a **HWFLY** modchip to hack your Switch.
	- *Please be aware that **SX Core** and **SX Lite** modchips are extremely hard to find due to being discontinued and require to be soldered onto the motherboard.*
      - ***HWFLY** modchips are easier to find , however they are extremely expensive, ranging from 86$ ~ $250 and also require to be soldered onto the motherboard.*

***
If you are still uncertain whether your Switch is unpatched or patched, then you can check by [clicking here](https://ismyswitchpatched.com/) and inputting your serial number for your Switch.
You can find your serial number either underneath your Switch, on the box of your Switch, or you can go into your `Settings`, then pick `System`, and then pick `Serial Numbers`. Your console's serial number will be displayed there. 





### How to avoid getting banned
***
Getting your Switch banned will lead to you not being able to:
- Access the eShop.
- Access Nintendo Switch Online.
- Getting updates to games from the eShop.

!!! note Your Switch getting banned will not cause your Nintendo account to get banned.
!!! warning If you cheat in online games and your Switch gets banned, it is highly likely for your account to get banned as well.

##### What will get you banned
***
- Modifying Online Games.
	- Cheating in online games.
    - Using layeredfs mods with online games.
    - Editing save files for online games.
- Making modifications to your SysNAND.
    - Installing **nsp**/**nsz**/**xci**/**xcz** files.
		- Installing homebrew forwarders will get you banned.
		- Installing game backups will get you banned.
    - Running nro's that modify your SysNAND (Tinfoil Installer, etc.)
	- Using a custom profile picture.
- Doing the above points on Emunand without using:
	- Exosphere/Incognito.
	- DNS MITM.


!!!note If you use EmuNAND and you accidentally boot into your SysNAND while using CFW, you will not get banned. Simply booting into SysNAND with CFW will not get you banned.

##### What MIGHT not get you banned
***
- Using CFW.
- Using cheats in offline games.
- Running homebrew.
- Using layeredfs mods for offline games.

!!!warning It is not recommended to run CFW without using Emunand and Incognito or Exosphere.

##### How to avoid getting banned
***
There are four methods that you may use in order to avoid getting banned:
- [EmuNAND](https://rentry.org/SwitchTerminology#emunand)
- [Exosphere](https://rentry.org/ExosphereDNSMITM#exosphere)
- [DNS MITM](https://rentry.org/ExosphereDNSMITM#dns-mitm)
- [Incognito](https://rentry.org/SettingUpIncognito)

It is highly recommended that you use Exosphere or Incognito alongside EmuNAND and DNS MITM.

Once you are using the above methods, you will be protected against getting your Switch banned.

!!!warning Nintendo always makes changes to their system and these protection measures could always become outdated, so make sure to visit the guide frequently in order to stay up to date with the latest protection measures.

!!!info Even if you are already banned, it is still recommended that you use these protection measures. While being banned won't allow you to get eshop updates or go online, you'll still receive firmware updates. So in order to prevent accidental updates and prevent the update nag, please use the mentioned protection measures in this guide.

!!!danger DNS MITM works only in atmosphere 0.18.1 and up. If you use an older atmosphere version, DNS MITM will not work.


##### DO NOT USE 90DNS
***
Why you should not use 90DNS:
* Relies on an external server
	* You have no control over what gets blocked.
	* 90DNS can affect your download speeds.
* Protection is not retained when switching networks
	* If you switch to a different WIFI network, your DNS settings will be removed
		and you risk being banned.

Why DNS MITM is better than 90DNS:
* Blocking relies on your device and not an external server.
* Gives you control over what gets blocked.
* Protection is retained when switching networks.

Stick to MITM DNS/Incognito/Exosphere, and do not use 90DNS!

##### Is it possible to unban a Switch?
***
No. 
A banned Switch is permanently banned. 
There are no methods currently for unbanning a Switch





### How to launch a payload
***

##### Erista (Unpatched):
***
**Step 1.**
- You can either buy a jig off eBay, Amazon, Aliexpress, Wish or make your own with tinfoil.
	- The tinfoil method is not recommended as it could damage the pins in your rail.
	- If you have AutoRCM on then you can skip this step.
- Power down your console and place the jig inside of the right railing. 

- Hold the **(VOL +)** button and press the ![power](https://i.imgur.com/rrXTJNZ.png) button.
	- Your Switch display will be black, do not panic, this is normal and 
		means that you're booted into [RCM](https://rentry.org/SwitchTerminology#rcm).

**Step 2.**

- Pick the type of payload injector you want to use:
	- [TegraRcmGUI](https://github.com/eliboa/TegraRcmGUI/releases/latest) (**Windows**)
	- [NS-USBloader](https://github.com/developersu/ns-usbloader/releases/latest) (**Windows, Mac, & Linux**)
	- [Web Fusée Launcher](https://switch.exploit.fortheusers.org/) (**Mac & Linux**)
	- [Rekado](https://github.com/MenosGrante/Rekado/releases/latest) (**Android**)
	- [NXBoot](https://mologie.github.io/nxboot) (**iOS** , requires a **jailbroken** device)
	- [Portable Payload Injector](https://is.gd/psX3hv) (Can be bought on **Amazon**, **eBay**, **Aliexpress**, and **Wish**)

!!!info Make sure to follow the documentation for your desired payload injector.

**Step 3.**
- Pick your desired payload with your payload injector of choice and connect your Switch to your PC.
	- If you use a portable payload injector, then you only need to connect your payload injector into your Switch.

- You should now launch into your desired payload.

##### Erista (IPatched) / Mariko:
***
!!!info Mariko Users:
	**Please ensure that the payload supports your device. 
	Not all payloads support Mariko devices. 
	If you are unsure on what type of device you may have, [click here](https://rentry.org/IsMySwitchPatched).**

To launch payloads on an **IPatched Erista** or a **Mariko** unit, then you must use either:
* An **SX Core/ SX Lite** modchip
***OR***
* A **HWFLY** modchip.

**Step 1:**
- Download the following files and extract them into the root of your SD card:
	- [SX Gear](https://web.archive.org/web/20210217231219/https://sx.xecuter.com/download/SX_Gear_v1.1.zip) 
		- Skip this file if you use an **SX Core / SX lite** with **Spacecraft-NX** or a **HWFLY** modchip
	- [Hekate](https://github.com/CTCaer/hekate/releases/latest)

**Step 2:**
- Rename `hekate_ctcaer_x.x.x.bin` to payload.bin.
- Place your desired payload in `/bootloader/payloads/`.

**Step 3:**
- Press the ![power](https://i.imgur.com/rrXTJNZ.png) button and you will launch into Hekate.
- Once in Hekate, press the `payloads` option and pick your desired payload.
- You will now boot into your desired payload.




### How to create/restore a NAND backup
***
It is highly recommended to create a NAND backup before you start using your CFW setup. 
You can make a NAND backup through two methods:
* [**Switch Method**]()
	* This is the recommended method.
* [**PC Method**]()
	* This method is recommended for those who have a small SD card and are unable to create a NAND backup due to their SD card storage size.

***
!!! warning Before you begin, please ensure that you know how to launch payloads on your device by reading [this](https://rentry.org/SwitchPayloadLaunch) guide.


#### Switch Method
##### What you'll need:
***
* [Hekate](https://github.com/CTCaer/hekate/releases/latest/)

##### Creating a NAND backup:
***
1. Extract `hekate_ctcaer_x.x.x_Nyx_x.x.x.zip` into your SD card.
2. Boot into Hekate.
3. Press on the `Tools` tab.
4. Pick the `Backup eMMC` option.
5. Pick the `eMMC RAW GPP` option.
6. Once done, press `Close`,  and then pick `Backup eMMC` again.
7. Pick the `eMMC BOOT0 & BOOT1` option.
8. Once done, press `Close` and shut down your Switch.

Your backup will now be in your `backup` folder inside of your SD card.
It is recommended that you move your `backup` folder somewhere on your PC, or on a cloud storage. 

##### Restoring a NAND backup:
***
Make sure your `backup` folder is in the root of your SD card with your NAND backup inside of it.
 
1. Move all the files and folders inside of your `/backup/xxxxxxxx/` folder to your `/backup/xxxxxxxx/restore/` folder.
2. Boot into Hekate.
3. Press on the `Tools` tab.
4. Pick the `Restore eMMC` option.
5. Pick the `eMMC RAW GPP` option.
6. Once done press `Close` and go back to the `Restore eMMC` option.
7. Pick `eMMC BOOT0 & BOOT1`.
8. Once done, press `Close` and reboot your Switch.

[]()
[]()

#### PC Method

##### What you'll need:
***
* [Hekate](https://github.com/CTCaer/hekate/releases/latest/)
* [NxNandManager](https://github.com/eliboa/NxNandManager/releases/latest/)

##### Creating a NAND backup:
***

1. Create a folder on your PC named `Backup`.
2. Extract `NxNandManager_vx.x.x_x64.zip` to your desktop.
3. Extract `hekate_ctcaer_x.x.x_Nyx_x.x.x.zip` to your SD card.
4. Boot into **Hekate**.
5. Connect your **Switch** to your PC.
6. Select the `Tools` tab.
7. Select `USB Tools`.
8. Select `eMMC RAW GPP`.
9. Launch **NxNandManager**.
10. Select `File` and select `Open drive`.
11. Select `eMMC GPP hekate`.
12. Select `FULL DUMP` select the `Backup` folder you made as the target folder.
13. Once the progress bar says `RAWNAND dumped & verified` close the window and disconnect your **Switch**.
14. In **Hekate**, select `Close` and select `Backup eMMC`. 
15. Select `eMMC BOOT0 & BOOT1`.
16. Once done, select `Close` and go back to the `Tools` tab.
17. Select `USB Tools` and select `SD Card`.
	* You will now have access to your SD card. 
18. On your SD card, copy your `/backup/xxxxxxxx/` folder your `Backup` folder on your PC:


##### Restoring a NAND backup:
***
1. Launch into **Hekate**.
2. Connect your **Switch** to your PC.
3. Select the `Tools` tab.
4. Select `USB Tools`.
5. Ensure that the `Read-Only` option is set to `OFF`.
6. Select `eMMC RAW GPP`.
7. Launch **NxNandManager**.
8. Select `File` and select `Open drive`.
9. Select `eMMC GPP hekate`.
10. Select `FULL or PARTIAL RESTORE`.
12. Select `RAWNAND.bin` from your `Backup` folder on your PC.
13. Select `Yes`. 
14. Once done, disconnect your **Switch**.
15. Pick `SD Card` in **Hekate** and connect your **Switch** to your PC.
	* You will now have access to your SD card. 
16. Copy your `/backup/xxxxxxxx/`  folder to your `backup` folder on your PC.
17. Move all the files and folders inside of your `/backup/xxxxxxxx/` folder to your `/backup/xxxxxxxx/restore/` folder.
18. Disconnect your **Switch**, select `Close` go back to the `Tools` tab.
19. Select `Restore eMMC` and select `eMMC BOOT0 & BOOT1`.
20. Once done, you may boot back into your **Switch**.





### How to dump your device's keys
***


!!! warning Before you begin, please ensure that you know how to launch payloads on your device by reading [this](https://rentry.org/SwitchPayloadLaunch) guide.


#### What you'll need:
***
* [Lockpick_RCM](https://github.com/shchmue/Lockpick_RCM/releases/latest/download/Lockpick_RCM.bin)

##### Dumping device keys:
***
1. Boot into `Lockpick_RCM.bin`.
2. Once in **Lockpick**, select **Dump from SysNAND**.
	* You may use the **(VOL +)** and **(VOL -)** buttons to scroll up and down and
		use the ![power](https://i.imgur.com/rrXTJNZ.png) button to confirm your desired selection.
3. Once done, press the ![power](https://i.imgur.com/rrXTJNZ.png) button to go back to the main menu.
4. Scroll down to the `Power off` option, and press the ![power](https://i.imgur.com/rrXTJNZ.png) button to shut down your console.
5. Connect your SD card to your PC.
	* Please read the following [guide](https://rentry.org/SDCardViaUSB) to learn how to access your SD card 
		via USB.
6. Access the the `/switch/` folder on your SD card, and copy the following files to your PC:
	* `prod.keys`
	* `title.keys`
	**Mariko only:**
	* `partialaes.keys`
!!!warning	Backup your keys!
	**Please ensure that you backup these files somewhere safe. 
	Your device's keys have different use cases ranging from being used with emulators to unbricking your device. **


[]()
[]()

***