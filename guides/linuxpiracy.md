# ► Linux Game Installation
* **[Linux Game Installation Guide](https://github.com/reggiiie/free-media-linux)** - *Run Windows Games on Linux*

# ► Linux Downloads
* **[de.linuxgame.cn](https://de.linuxgame.cn/)** - *Linux Games*  
* **[The KDE Games Center](https://games.kde.org/)** - *Linux Games*
* **[Zamunda](https://zamunda.net/login.php?returnto=%2Fgames.php)** - *Linux Games / Use [Translator](https://addons.mozilla.org/en-US/firefox/addon/traduzir-paginas-web/)*
* **[Link Scrape](https://github.com/reggiiie/link-scrape)** - *Linux Game Search Script*
* **[Torrminatorr](https://torrminatorr.com/)** - *Direct Download Site with Linux Games*

# ► Linux Emulators
* **[GamingPiracyGuide - emulation-roms](https://github.com/nbats/FMHYedit/blob/main/GamingPiracyGuide.md#emulation-roms)** - *Link to Main Thread that Contains ROMs*
* **[Retroarch](https://www.retroarch.com/index.php?page=linux-instructions)** - *Multisystem Emulator*
* **[Yuzu](https://yuzu-emu.org/downloads/)** - *Nintendo Switch Emulator*
* **[Yuzu - Archlinux](https://aur.archlinux.org/packages/yuzu-ea-bin)** - *AUR Link to Yuzu Early Access Binary*
* **[ZDoom](https://zdoom.org/downloads)** - *Port of DOOM Game Engine*
* **[Brutal Doom](https://aur.archlinux.org/packages/brutal-doom/)** - *AUR Package of GZDoom with Brutal Doom Mod Installed*
# ► Linux Tools
* **[QBittorrent](https://www.qbittorrent.org/download.php)** - *Great Torrent Client*
* **[JDownloader2](https://jdownloader.org/jdownloader2)** - *Download Manager*
* **[JDownloader2 Snap](https://snapcraft.io/jdownloader2)** - *JDownlader2 on SnapCraft*
* **[ffmpeg](https://www.ffmpeg.org/)** - *Utility for Transcoding Videos*
* **[HandBrake](https://handbrake.fr/)** - *GUI Video Transcoder*


# ► Linux Privacy
* **[UFW Archlinux](https://wiki.archlinux.org/title/Uncomplicated_Firewall)** - *ArchWiki Entry for Uncomplicated Firewall*
* **[UFW Ubuntu](https://help.ubuntu.com/community/UFW)** - *Ubuntu Help Entry for Uncomplicated Firewall*
* **[OpenVPN](https://openvpn.net/community-downloads/)** - *Open Source VPN client*
* **[OpenVPN Archlinux](https://wiki.archlinux.org/title/OpenVPN)** - *ArchWiki Entry for OpenVPN*
* **[Namespaced OpenVPN](https://github.com/slingamn/namespaced-openvpn)** - *Run OpenVPN in it's Separate Namespace so Only Apps Running in that Namespace Connect to VPN*
* **[KeepassXC](https://keepassxc.org/)** - *Password Manager*
* **[Arkenfox/User.js](https://github.com/arkenfox/user.js/)** - *Privacy Focused Firefox Settings*